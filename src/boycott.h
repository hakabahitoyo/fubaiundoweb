#ifndef BOYCOTT_H
#define BOYCOTT_H


#include <string>
#include <vector>
#include "picojson.h"


namespace fubaiundoweb {


using namespace std;


class Boycott {
public:
	uint64_t id;
	string corporation;
	string president;
	string address;
	string phone;
	vector <string> products;
public:
	Boycott () = default;
	Boycott (string json);
	Boycott (picojson::value json_value);
public:
	void from_json_value (picojson::value value);
	string to_json () const;
};


}; /* namespace fubaiundoweb { */


#endif /* #ifndef BOYCOTT_H */

