async function fill () {
	const id = window.location.search.split ('?') [1]
	try {
		const response = await fetch ('/api/boycotts')
		const json = await response.json ()
		for (const boycott of json) {
			if (boycott.id === id) {
				document.getElementById ('input-corporation').value = boycott.corporation
				document.getElementById ('input-president').value = boycott.president
				document.getElementById ('input-address').value = boycott.address
				document.getElementById ('input-phone').value = boycott.phone
				document.getElementById ('textarea-products').value = boycott.products.join ("\n")
			}
		}
	} catch (error) {
		/* Do nothing. */
	}
}


async function onOK () {
	const id = window.location.search.split ('?') [1]
	const corporation = document.getElementById ('input-corporation').value
	const president = document.getElementById ('input-president').value
	const address = document.getElementById ('input-address').value
	const phone = document.getElementById ('input-phone').value
	const productsString = document.getElementById ('textarea-products').value
	const productsButMayEmpty = productsString.split ("\n")
	const products = []
	for (const product of productsButMayEmpty) {
		if (0 < product.length) {
			products.push (product)
		}
	}
	if (corporation.length === 0) {
		alert ('「企業またはグループ」は必須項目です。')
		return
	}
	const post = JSON.stringify ({
		id: id,
		corporation: corporation,
		president: president,
		address: address,
		phone, phone,
		products, products
	})
	try {
		await fetch ('/api/edit', {method: 'POST', body: post})
		window.location = '/web'
	} catch (error) {
		/* Do nothing. */
	}
}


window.addEventListener ('load', async function () {
	await fill ()

	document.getElementById ('button-ok').addEventListener ('click', onOK, false)
	
	document.getElementById ('button-cancel').addEventListener ('click', function () {
		window.location = '/web'
	}, false)
}, false) /* window.addEventListener ('load', function () { */


