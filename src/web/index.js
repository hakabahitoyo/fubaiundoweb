async function renderBoycotts () {
	try {
		const response = await fetch ('/api/boycotts')
		const json = await response.json ()
		
		document.getElementById ('placeholder-boycotts').innerHTML = ''
		
		for (const boycott of json) {
			const div = document.createElement ('div')
			div.setAttribute ('id', 'boycott-' + boycott.id)

			const h3 = document.createElement ('h3')
			h3.innerText = boycott.corporation + '不買運動'
			div.append (h3)

			const pToolbox = document.createElement ('p')
			
			const buttonEdit = document.createElement ('input')
			buttonEdit.setAttribute ('type', 'button')
			buttonEdit.setAttribute ('style', 'width: 96px; height: 32px;')
			buttonEdit.setAttribute ('value', '編集')
			buttonEdit.addEventListener ('click', function () {
				window.location = '/web/edit.html?' + boycott.id
			}, false)
			pToolbox.appendChild (buttonEdit)
			
			const buttonGoTop = document.createElement ('input')
			buttonGoTop.setAttribute ('type', 'button')
			buttonGoTop.setAttribute ('style', 'width: 96px; height: 32px;')
			buttonGoTop.setAttribute ('value', '上へ')
			buttonGoTop.addEventListener ('click', async function () {
				try {
					await fetch ('/api/gotop', {'method': 'post', 'body': boycott.id})
					renderBoycotts ()
				} catch (error) {
					/* Do nothing. */
				}
			}, false)
			pToolbox.appendChild (buttonGoTop)
			
			const buttonGoBottom = document.createElement ('input')
			buttonGoBottom.setAttribute ('type', 'button')
			buttonGoBottom.setAttribute ('style', 'width: 96px; height: 32px;')
			buttonGoBottom.setAttribute ('value', '下へ')
			buttonGoBottom.addEventListener ('click', async function () {
				try {
					await fetch ('/api/gobottom', {'method': 'post', 'body': boycott.id})
					renderBoycotts ()
				} catch (error) {
					/* Do nothing. */
				}
			}, false)
			pToolbox.appendChild (buttonGoBottom)
			
			div.append (pToolbox)

			const pPresident = document.createElement ('p')
			pPresident.innerText = '代表者: ' + boycott.president
			div.append (pPresident)
			
			const pAddress = document.createElement ('p')
			pAddress.innerText = '所在地: ' + boycott.address
			div.append (pAddress)
			
			const pPhone = document.createElement ('p')
			pPhone.innerText = '電話番号: ' + boycott.phone
			div.append (pPhone)
			
			const pProducts = document.createElement ('p')
			pProducts.innerText = '製品・サービス・ブランド・関連会社:'
			div.append (pProducts)
			
			const ul = document.createElement ('ul')
			
			for (const product of boycott.products) {
				const li = document.createElement ('li')
				li.innerText = product
				ul.append (li)
			}
			
			div.append (ul)
			
			document.getElementById ('placeholder-boycotts').append (div)
		}
	} catch (error) {
		document.getElementById ('placeholder-boycotts').innerText = '壊れた。'
	}
}


window.addEventListener ('load', async function () {

document.getElementById ('button-ok').addEventListener ('click', async function () {
	const corporation = document.getElementById ('input-corporation').value
	const president = document.getElementById ('input-president').value
	const address = document.getElementById ('input-address').value
	const phone = document.getElementById ('input-phone').value
	const productsString = document.getElementById ('textarea-products').value
	const productsButMayEmpty = productsString.split ("\n")
	const products = []
	for (const product of productsButMayEmpty) {
		if (0 < product.length) {
			products.push (product)
		}
	}
	if (corporation.length === 0) {
		alert ('「企業またはグループ」は必須項目です。')
		return
	}
	const post = JSON.stringify ({
		corporation: corporation,
		president: president,
		address: address,
		phone, phone,
		products, products
	})
	try {
		await fetch ('/api/create', {method: 'POST', body: post})
		document.getElementById ('input-corporation').value = ''
		document.getElementById ('input-president').value = ''
		document.getElementById ('input-address').value = ''
		document.getElementById ('input-phone').value = ''
		document.getElementById ('textarea-products').value = ''
		await renderBoycotts ()
	} catch (error) {
		/* Do nothing. */
	}
}, false)

await renderBoycotts ()

}, false) /* window.addEventListener ('load', function () { */

