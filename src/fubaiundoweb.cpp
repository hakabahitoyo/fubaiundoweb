
#include <exception>
#include <deque>
#include <random>
#include <sstream>
#include <cstdlib>
#include <fstream>
#include <ctime>
#include <filesystem>

#include "mongoose.h"
#include "picojson.h"
#include "fubaiundoweb.h"


using namespace std;
using namespace fubaiundoweb;


/* Global */


static struct mg_mgr mongoose_manager;
static default_random_engine random_engine {random_device {} ()};
static string file_path {string {"/var/lib/fubaiundoweb/fubaiundoweb.jsonl"}};
static string log_path {string {"/var/lib/fubaiundoweb/log.jsonl"}};
static uintmax_t log_limit = 1'000'000'000;


/* Util */


static string make_string (struct mg_str in)
{
	return string {in.ptr, in.len};
}


static bool starts_with (string x, string y)
{
	return
		y.size () <= x.size ()
		&& x.substr (0, y.size ()) == y;
}


static bool ends_with (string x, string y)
{
	return
		y.size () <= x.size ()
		&& x.substr (x.size () - y.size ()) == y;
}


static string escape_json (string in)
{
	string out;
	for (auto c: in) {
		if (c == '\n') {
			out += string {"\\n"};
		} else if (c == '\r') {
			out += string {"\\r"};
		} else if (c == '\t') {
			out += string {"\\t"};
		} else if (c == '"') {
			out += string {"\\\""};
		} else if (c == '\\') {
			out += string {"\\\\"};
		} else if (0x00 <= c && c < 0x20) {
			out += string {"�"};
		} else {
			out.push_back (c);
		}
	}
	return out;
}


/* Server */


static string construct_404_page ()
{
	string html = string {
		"<!DOCTYPE html>\r\n"
		"<meta name=\"viewport\" content=\"width=device-width\">\r\n"
		"<title>404</title>\r\n"
		"<p>404</p>\r\n"
	};

	return html;
}


static deque <Boycott> read_boycotts_from_file ()
{
	deque <Boycott> boycotts;
	FILE * file = fopen (file_path.c_str(), "r");
	string cpp_buffer;
	char c_buffer [0x1000];
	for (; ; ) {
		if (feof (file)) {
			break;
		}
		fgets (c_buffer, 0x1000, file);
		if (feof (file)) {
			break;
		}
		cpp_buffer += string {c_buffer};
		if (cpp_buffer.back () == '\n') {
			try {
				Boycott boycott {cpp_buffer};
				boycotts.push_back (boycott);
			} catch (exception &) {
				/* Do nothing, */
			}
			cpp_buffer.clear ();
		}
	}
	fclose (file);
	return boycotts;
}


static void write_boycotts_to_file (deque <Boycott> boycotts)
{
	ofstream file {file_path};
	for (auto boycott: boycotts) {
		file << boycott.to_json () << endl;
	}
}


static void write_log (string method, deque <Boycott> boycotts, string x_forwarded_for)
{
	Action action;
	action.timestamp = time (nullptr);
	action.method = method;
	action.x_forwarded_for = x_forwarded_for;
	action.boycotts = boycotts;
	string action_string = action.to_json ();

	if (log_limit < action_string.size ()) {
		abort ();
	}

	ofstream file {log_path, std::ios::app};
	file << action_string << endl;

	uintmax_t log_file_size = file_size (std::filesystem::path {log_path});
	if (log_limit < log_file_size) {
		abort ();
	}
}


static void create_boycott (string post, string x_forwarded_for)
{
	picojson::value post_value;
	string json_parse_error = picojson::parse (post_value, post);
	if (! json_parse_error.empty ()) {
		throw (exception {});
	}
	auto post_object = post_value.get <picojson::object> ();
	string corporation = post_object.at (string {"corporation"}).get <string> ();
	string president = post_object.at (string {"president"}).get <string> ();
	string address = post_object.at (string {"address"}).get <string> ();
	string phone = post_object.at (string {"phone"}).get <string> ();
	auto products_array = post_object.at (string {"products"}).get <picojson::array> ();
	vector <string> products;
	for (auto product_value: products_array) {
		products.push_back (product_value.get <string> ());
	}

	if (corporation.empty ()) {
		throw (exception {});
	}
	
	Boycott boycott;
	uniform_int_distribution <uint64_t> distribution {};
	boycott.id = distribution (random_engine);
	boycott.corporation = corporation;
	boycott.president = president;
	boycott.address = address;
	boycott.phone = phone;
	boycott.products = products;
	
	deque <Boycott> boycotts = read_boycotts_from_file ();

	boycotts.push_front (boycott);
	
	write_boycotts_to_file (boycotts);
	write_log (string {"create"}, boycotts, x_forwarded_for);
}


static void edit_boycott (string post, string x_forwarded_for)
{
	picojson::value post_value;
	string json_parse_error = picojson::parse (post_value, post);
	if (! json_parse_error.empty ()) {
		throw (exception {});
	}
	auto post_object = post_value.get <picojson::object> ();
	string id_string = post_object.at (string {"id"}).get <string> ();
	stringstream id_stream {id_string};
	uint64_t id;
	id_stream >> id;
	string corporation = post_object.at (string {"corporation"}).get <string> ();
	string president = post_object.at (string {"president"}).get <string> ();
	string address = post_object.at (string {"address"}).get <string> ();
	string phone = post_object.at (string {"phone"}).get <string> ();
	auto products_array = post_object.at (string {"products"}).get <picojson::array> ();
	vector <string> products;
	for (auto product_value: products_array) {
		products.push_back (product_value.get <string> ());
	}

	if (corporation.empty ()) {
		throw (exception {});
	}
	
	deque <Boycott> boycotts = read_boycotts_from_file ();

	for (auto & boycott: boycotts) {
		if (id == boycott.id) {
			boycott.corporation = corporation;
			boycott.president = president;
			boycott.address = address;
			boycott.phone = phone;
			boycott.products = products;
		}
	}
	
	write_boycotts_to_file (boycotts);
	write_log (string {"edit"}, boycotts, x_forwarded_for);
}


static void go_top (string post, string x_forwarded_for)
{
	stringstream id_stream {post};
	uint64_t id;
	id_stream >> id;

	deque <Boycott> old_boycotts = read_boycotts_from_file ();

	deque <Boycott> new_boycotts;

	Boycott moved_boycott;

	for (auto boycott: old_boycotts) {
		if (id == boycott.id) {
			moved_boycott = boycott;
		} else {
			new_boycotts.push_back (boycott);
		}
	}
	
	new_boycotts.push_front (moved_boycott);
	
	write_boycotts_to_file (new_boycotts);
	write_log (string {"go_top"}, new_boycotts, x_forwarded_for);
}


static void go_bottom (string post, string x_forwarded_for)
{
	stringstream id_stream {post};
	uint64_t id;
	id_stream >> id;

	deque <Boycott> old_boycotts = read_boycotts_from_file ();

	deque <Boycott> new_boycotts;

	Boycott moved_boycott;

	for (auto boycott: old_boycotts) {
		if (id == boycott.id) {
			moved_boycott = boycott;
		} else {
			new_boycotts.push_back (boycott);
		}
	}
	
	new_boycotts.push_back (moved_boycott);
	
	write_boycotts_to_file (new_boycotts);
	write_log (string {"go_bottom"}, new_boycotts, x_forwarded_for);
}


static void cb (struct mg_connection *c, int ev, void *ev_data, void *fn_data) {
	if (ev == MG_EV_HTTP_MSG) {
		auto http_message = reinterpret_cast <struct mg_http_message *> (ev_data);
		
		string x_forwarded_for = make_string (* mg_http_get_header (http_message, "X-Forwarded-For"));
		
		if (make_string (http_message->method) == string {"GET"}) {
			if (make_string (http_message->uri) == string {"/"}) {
				mg_http_reply (
					c,
					200,
					"Content-Type: text/html\r\n",
					"%s",
					"<meta http-equiv=\"refresh\" content=\"0; URL=/web\">"
				);
			} else if (starts_with (make_string (http_message->uri), string {"/web"})) {
				struct mg_http_serve_opts opts {
					.root_dir = ".",
				};
				mg_http_serve_dir (c, http_message, & opts);
			} else if (make_string (http_message->uri) == string {"/api/boycotts"}) {
				auto boycotts = read_boycotts_from_file ();
				string boycotts_json;
				boycotts_json += string {"["};
				for (auto boycott = boycotts.begin (); boycott != boycotts.end (); boycott ++) {
					if (boycott != boycotts.begin ()) {
						boycotts_json += string {","};
					}
					boycotts_json += boycott->to_json ();
				}
				boycotts_json += string {"]"};
				mg_http_reply (
					c,
					200,
					"Content-Type: application/json\r\n",
					"%s",
					boycotts_json.c_str ()
				);
			} else {
				mg_http_reply (
					c,
					404,
					"Content-Type: text/html\r\n",
					"%s",
					construct_404_page ().c_str ()
				);
			}
		} else if (make_string (http_message->method) == string {"POST"}) {
			if (make_string (http_message->uri) == string {"/api/create"}) {
				try {
					create_boycott (make_string (http_message->body), x_forwarded_for);
					mg_http_reply (
						c,
						200,
						"Content-Type: text/html\r\n",
						"%s",
						"OK."
					);
				} catch (exception &) {
					mg_http_reply (
						c,
						500,
						"Content-Type: text/html\r\n",
						"%s",
						"Internal server error."
					);
				}
			} else if (make_string (http_message->uri) == string {"/api/edit"}) {
				try {
					edit_boycott (make_string (http_message->body), x_forwarded_for);
					mg_http_reply (
						c,
						200,
						"Content-Type: text/html\r\n",
						"%s",
						"OK."
					);
				} catch (exception &) {
					mg_http_reply (
						c,
						500,
						"Content-Type: text/html\r\n",
						"%s",
						"Internal server error."
					);
				}
			} else if (make_string (http_message->uri) == string {"/api/gotop"}) {
				try {
					go_top (make_string (http_message->body), x_forwarded_for);
					mg_http_reply (
						c,
						200,
						"Content-Type: text/html\r\n",
						"%s",
						"OK."
					);
				} catch (exception &) {
					mg_http_reply (
						c,
						500,
						"Content-Type: text/html\r\n",
						"%s",
						"Internal server error."
					);
				}
			} else if (make_string (http_message->uri) == string {"/api/gobottom"}) {
				try {
					go_bottom (make_string (http_message->body), x_forwarded_for);
					mg_http_reply (
						c,
						200,
						"Content-Type: text/html\r\n",
						"%s",
						"OK."
					);
				} catch (exception &) {
					mg_http_reply (
						c,
						500,
						"Content-Type: text/html\r\n",
						"%s",
						"Internal server error."
					);
				}
			} else {
				mg_http_reply (
					c,
					404,
					"Content-Type: text/html\r\n",
					"%s",
					construct_404_page ().c_str ()
				);
			}
		}
	}
}


/* Main */


int main (int argc, char *argv []) {
	mg_mgr_init (& mongoose_manager);
	mg_http_listen (& mongoose_manager, "http://localhost:8100", cb, nullptr);
	for (; ; ) {
		mg_mgr_poll (& mongoose_manager, 10);
	}
	mg_mgr_free (& mongoose_manager);
	return 0;
}

