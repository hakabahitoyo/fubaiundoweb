#include <cstdlib>
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include "action.h"
#include "boycott.h"


using namespace fubaiundoweb;
using namespace std;


/* Global */


static string file_path {string {"/var/lib/fubaiundoweb/fubaiundoweb.jsonl"}};
static string log_path {string {"/var/lib/fubaiundoweb/log.jsonl"}};


static vector <Action> read_actions_from_log ()
{
	vector <Action> actions;
	FILE * file = fopen (log_path.c_str(), "r");
	string cpp_buffer;
	char c_buffer [0x1000];
	for (; ; ) {
		if (feof (file)) {
			break;
		}
		fgets (c_buffer, 0x1000, file);
		if (feof (file)) {
			break;
		}
		cpp_buffer += string {c_buffer};
		if (cpp_buffer.back () == '\n') {
			try {
				Action action {cpp_buffer};
				actions.push_back (action);
			} catch (exception &) {
				/* Do nothing, */
			}
			cpp_buffer.clear ();
		}
	}
	fclose (file);
	return actions;
}


static void write_boycotts_to_file (deque <Boycott> boycotts)
{
	ofstream file {file_path};
	for (auto boycott: boycotts) {
		file << boycott.to_json () << endl;
	}
}


int main (int argc, char ** argv)
{
	if (argc <= 1) {
		cout << "Usage: ./fubaiundoweb-rollback timestamp" << endl;
		exit (0);
	}
	string timestamp_string {argv [1]};
	
	stringstream timestamp_stream {timestamp_string};
	time_t timestamp;
	timestamp_stream >> timestamp;
	
	vector <Action> actions = read_actions_from_log ();
	
	for (auto action: actions) {
		if (timestamp == action.timestamp) {
			write_boycotts_to_file (action.boycotts);
			return 0;
		}
	}
	
	cerr << "Timestamp " << timestamp << " does not match." << endl;
	
	return 0;
}
