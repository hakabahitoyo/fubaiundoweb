#include <sstream>
#include "boycott.h"
#include "picojson.h"


using namespace fubaiundoweb;
using namespace std;


/* Util */


static string escape_json (string in)
{
	string out;
	for (auto c: in) {
		if (c == '\n') {
			out += string {"\\n"};
		} else if (c == '\r') {
			out += string {"\\r"};
		} else if (c == '\t') {
			out += string {"\\t"};
		} else if (c == '"') {
			out += string {"\\\""};
		} else if (c == '\\') {
			out += string {"\\\\"};
		} else if (0x00 <= c && c < 0x20) {
			out += string {"�"};
		} else {
			out.push_back (c);
		}
	}
	return out;
}


/* Member functions */


Boycott::Boycott (string json)
{
	picojson::value json_value;
	string json_parse_error = picojson::parse (json_value, json);
	if (! json_parse_error.empty ()) {
		throw (exception {});
	}
	from_json_value (json_value);
}


Boycott::Boycott (picojson::value json_value)
{
	from_json_value (json_value);
}


void Boycott::from_json_value (picojson::value json_value)
{
	auto json_object = json_value.get <picojson::object> ();
	string id_string = json_object.at (string {"id"}).get <string> ();
	stringstream id_stream {id_string};
	id_stream >> id;
	corporation = json_object.at (string {"corporation"}).get <string> ();
	president = json_object.at (string {"president"}).get <string> ();
	address = json_object.at (string {"address"}).get <string> ();
	phone = json_object.at (string {"phone"}).get <string> ();
	auto products_array = json_object.at (string {"products"}).get <picojson::array> ();
	for (auto product_value: products_array) {
		products.push_back (product_value.get <string> ());
	}
}


string Boycott::to_json () const
{
	string json;
	json += string {"{"};
	stringstream id_stream;
	id_stream << id;
	json += string {"\"id\":\""} + escape_json (id_stream.str ()) + string {"\","};
	json += string {"\"corporation\":\""} + escape_json (corporation) + string {"\","};
	json += string {"\"president\":\""} + escape_json (president) + string {"\","};
	json += string {"\"address\":\""} + escape_json (address) + string {"\","};
	json += string {"\"phone\":\""} + escape_json (phone) + string {"\","};
	json += string {"\"products\":["};
	for (auto product = products.begin (); product != products.end (); product ++) {
		if (product != products.begin ()) {
			json += string {","};
		}
		json += string {"\""} + escape_json (* product) + string {"\""};
	}
	json += string {"]"};
	json += string {"}"};
	return json;
}

