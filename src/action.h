#ifndef ACTION_H
#define ACTION_H


#include <string>
#include <deque>
#include <ctime>
#include "boycott.h"
#include "picojson.h"


namespace fubaiundoweb {


using namespace std;


class Action {
public:
	time_t timestamp;
	string method;
	string x_forwarded_for;
	deque <Boycott> boycotts;
public:
	Action () = default;
	Action (string json);
	Action (picojson::value value);
public:
	void from_json_value (picojson::value value);
	string to_json () const;
};


}; /* namespace fubaiundoweb { */


#endif /* #ifndef ACTION_H */


