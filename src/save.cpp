#include <ctime>
#include <fstream>
#include "boycott.h"
#include "action.h"


using namespace fubaiundoweb;
using namespace std;


/* Global */


static string file_path {string {"/var/lib/fubaiundoweb/fubaiundoweb.jsonl"}};
static string log_path {string {"/var/lib/fubaiundoweb/log.jsonl"}};


static deque <Boycott> read_boycotts_from_file ()
{
	deque <Boycott> boycotts;
	FILE * file = fopen (file_path.c_str(), "r");
	string cpp_buffer;
	char c_buffer [0x1000];
	for (; ; ) {
		if (feof (file)) {
			break;
		}
		fgets (c_buffer, 0x1000, file);
		if (feof (file)) {
			break;
		}
		cpp_buffer += string {c_buffer};
		if (cpp_buffer.back () == '\n') {
			try {
				Boycott boycott {cpp_buffer};
				boycotts.push_back (boycott);
			} catch (exception &) {
				/* Do nothing, */
			}
			cpp_buffer.clear ();
		}
	}
	fclose (file);
	return boycotts;
}


int main (int argc, char ** argv)
{
	deque <Boycott> boycotts = read_boycotts_from_file ();
	Action action;
	action.timestamp = time (nullptr);
	action.method = string {"save"};
	action.x_forwarded_for = string {};
	action.boycotts = boycotts;
	ofstream file {log_path, std::ios::app};
	file << action.to_json () << endl;
	return 0;
}

