#include <sstream>
#include "action.h"


using namespace fubaiundoweb;
using namespace std;


/* Util */


static string escape_json (string in)
{
	string out;
	for (auto c: in) {
		if (c == '\n') {
			out += string {"\\n"};
		} else if (c == '\r') {
			out += string {"\\r"};
		} else if (c == '\t') {
			out += string {"\\t"};
		} else if (c == '"') {
			out += string {"\\\""};
		} else if (c == '\\') {
			out += string {"\\\\"};
		} else if (0x00 <= c && c < 0x20) {
			out += string {"�"};
		} else {
			out.push_back (c);
		}
	}
	return out;
}


Action::Action (string json)
{
	picojson::value json_value;
	string json_parse_error = picojson::parse (json_value, json);
	if (! json_parse_error.empty ()) {
		throw (exception {});
	}
	from_json_value (json_value);
}


Action::Action (picojson::value json_value)
{
	from_json_value (json_value);
}


void Action::from_json_value (picojson::value json_value)
{
	auto json_object = json_value.get <picojson::object> ();
	string timestamp_string = json_object.at (string {"timestamp"}).get <string> ();
	string method_string = json_object.at (string {"method"}).get <string> ();
	string x_forwarded_for_string = json_object.at (string {"x_forwarded_for"}).get <string> (); 
	auto boycotts_array = json_object.at (string {"boycotts"}).get <picojson::array> ();
	deque <Boycott> boycotts_deque;
	for (auto boycott_value: boycotts_array) {
		boycotts_deque.push_back (Boycott {boycott_value});
	}
	
	stringstream timestamp_stream;
	timestamp_stream.str (timestamp_string);
	timestamp_stream >> timestamp;
	method = method_string;
	x_forwarded_for = x_forwarded_for_string;
	boycotts = boycotts_deque;
}


string Action::to_json () const
{
	stringstream timestamp_stream;
	timestamp_stream << timestamp;
	string timestamp_string = timestamp_stream.str ();

	string boycotts_json;
	boycotts_json += string {"["};
	for (auto boycott = boycotts.begin (); boycott != boycotts.end (); boycott ++) {
		if (boycott != boycotts.begin ()) {
			boycotts_json += string {","};
		}
		boycotts_json += boycott->to_json ();
	}
	boycotts_json += string {"]"};

	string json;
	json += string {"{"};
	json += string {"\"timestamp\":\""} + timestamp_string + string {"\","};
	json += string {"\"method\":\""} + escape_json (method) + string {"\","};
	json += string {"\"x_forwarded_for\":\""} + escape_json (x_forwarded_for) + string {"\","};
	json += string {"\"boycotts\":"} + boycotts_json;
	json += string {"}"};

	return json;
}



