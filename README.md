# 不買運動ウェブ

https://fubaiundo.org

## Install

### Build and initialize

```
sudo apt install libssl-dev libcurl4-openssl-dev
cd src
make clean
make
sudo make install
```

### Write Caddyfile

Write following Caddyfile:

```
example.com {
	reverse_proxy :8100
}

```

## Run

In the screen command:

```
cd src
./fubaiundoweb
```

In the other screen:

```
sudo caddy run
```

## IP Address Ban

Write following Caddyfile:

```
example.com {
        @denied client_ip 198.51.100.0
        abort @denied
        reverse_proxy :8100
}
```

## Acknowledgements

* https://github.com/cesanta/mongoose
* https://github.com/kazuho/picojson

